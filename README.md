## Genomgång av projekt i form av restaurangkassa självbetjäning

### Använder media query för att emulera iPad. Justera webbläsarens storlek till mindre bredd.

![startsida](readme-ps/1.png)
![huvudmeny](readme-ps/2.png)
![matmeny](readme-ps/3.png)
![dryckesmeny](readme-ps/4.png)
![checkout](readme-ps/5.png)

### `npm start`
